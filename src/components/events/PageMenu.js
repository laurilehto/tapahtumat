import React from 'react'
import { Pagination, Container } from 'semantic-ui-react'

const PageMenu = (props) => {
    
    return (
        <Pagination as={Container}
            secondary
            activePage={props.activePage}
            totalPages={props.pages}
            onPageChange={props.onChange}
        />
    )
}

export default PageMenu