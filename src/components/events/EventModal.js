import React, { Component } from 'react'
import { Button, Header, Modal, Image } from 'semantic-ui-react'

import defaultImage from '../../images/default.jpg'

const makeImages = (url) => {
    const imagesObj = {}
    const images = [
        {
            size: '60x60',
            width: '60'
        },
        {
            size: '222x222',
            width: '222'
        },
        {
            size: '984x333',
            width: '984'
        },
    ]

    images.forEach(image => {
        let newUrl = url.replace(/60x60/g, image.size)
        newUrl = newUrl.replace(/60/g, image.width)
        imagesObj[image.width]=newUrl
    })
    return imagesObj
}

class EventModal extends Component {
    state = { open: false}

    close = () => this.setState({ open: false })

    open = () => this.setState({open: true})

    render() {
        const { title, description, time, timestamp, venue, city, image, link } = this.props.event
        const localDate = new Date(timestamp).toLocaleDateString('fi-FI')
        const images = makeImages(image)
        return (
            <Modal
                open={this.state.open}
                trigger = {
                    this.props.children
                }
                dimmer='inverted'
                centered={false}
                onClose={this.close}
                onOpen={this.open}
                >

                <Modal.Header>{localDate} {title}</Modal.Header>
                <Modal.Content image>
                    <Image 
                        wrapped 
                        size='medium' 
                        src={images['222']}
                        onError={e => e.target.src=defaultImage} />
                    <Modal.Description>
                        <Header size='large'>{title}</Header>
                        <Header size='medium'>{description}</Header>
                        <Header size='medium'>{localDate}   klo.{time}</Header>
                        <Header size='medium'>{venue}<br/>{city}</Header>
                    </Modal.Description> 
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={this.close} color='black' size='big'>Sulje</Button>
                    <Button href={link} target="_blank" color='purple' size='big'>Lisätietoa ja liput</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

export default EventModal