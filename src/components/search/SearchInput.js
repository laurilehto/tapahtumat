import React from 'react'
import { Dropdown, Menu, Icon } from 'semantic-ui-react'

const getOptions = (options, lan) => {
    const curOpt = []
    options.forEach((o)=>{
        const opt = {}
        opt.key = o.value
        opt.value = o.value
        opt.text = o.text[lan]
        curOpt.push(opt)
    })
    return curOpt
}

const SearchInput =(props)=> {
    const { hidden, style, name, placeholder, options, icon, title, value } = props
    const language = 'fi'
    
    return (
        <Menu.Item
            title={title}
            style={style} 
            onClick={props.onClick}
            hidden={hidden}>
            <Icon name={icon}/>
            <Dropdown
                fluid search selection clearable
                value={value}
                style={styles.dropdown}
                onChange={(e, data) => props.onChange(name,data.value)}
                placeholder={placeholder} 
                options={getOptions(options, language)}>
            </Dropdown>
        </Menu.Item>
    )
}

const styles = {
    dropdown: {
        borderStyle: 'none',
    }
}

export default SearchInput