import React, { Component } from 'react'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { geolocated } from 'react-geolocated'
import axios from 'axios'
import { Loader, Menu, Responsive } from 'semantic-ui-react'

import { Search } from '../../store/actions/searchActions'
import { Query } from '../../store/actions/queryActions'

import SearchFields from './SearchFields'
import MenuIcons from './MenuIcons'
 
class SearchBar extends Component {
    constructor(props) {
        super(props)

        this.state = {
            search: {
                date: '',
                city: '',
                category: '',
                text: ''
            },
            mobile: false
        }
        this.updateWindow = this.updateWindow.bind(this)
        this.getLocation = this.getLocation.bind(this)
        this.activeLocation = this.activeLocation.bind(this)
        this.clearSearch = this.clearSearch.bind(this)
        this.clearField = this.clearField.bind(this)
    }

    componentDidMount() {
        this.updateWindow();
        window.addEventListener("resize", this.updateWindow);
        this.getLocation()
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateWindow);
    }

    updateWindow() {
        if(window.innerWidth > 780){
            const mobile = false
            this.setState({ ...this.state, mobile });
        }    
    }

    getLocation() {
        const location = window.navigator && window.navigator.geolocation
        if (location) {
            location.getCurrentPosition((position) => {
                this.setState({
                    ...this.state,
                    location: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    }
                })
            }, (error) => {
                alert('no location set')
            })
        }
    }
    
    handleChange = (name, value) => {
        // console.log(name,value)
        const search = {}
        const keys = Object.keys(this.state.search)
        keys.forEach(key => {
            if(name===key){
                search[key]=value
            } else {
                search[key]=this.state.search[key]
            }
        })
        // console.log(search)
        if(search!==this.state.search){
            this.setState({ ...this.state, search })
            this.props.Query(search)
            this.props.Search(search)
        }
    }

    showMenu = () => this.setState({ search:this.state.search, mobile:!this.state.mobile })

    clearSearch = () => {
        const search= { date: '', city: '', category: '', text: '' }
        this.setState({ search, mobile:this.state.mobile })
        this.props.Query(search)
        this.props.Search(search)
    }

    activeLocation = () => {
        this.getLocation()
        if(this.state.location){
            const {latitude, longitude} = this.state.location
            const key = 'AIzaSyBsKNnCY_Sc4mn1ZwmiQm3tKTbWnSSY_HI'
            const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&sensor=true&key=${key}`
            axios.get(url)
            .then(res => {
                const city = res.data.plus_code.compound_code.split(' ')[1].replace(/(,)/g, "")
                const { mobile } = this.state
                const search = { ...this.state.search, city }
                this.props.Query(search)
                this.props.Search(search)
                // console.log(search) 
                this.setState({search, mobile})
            }).catch(err => console.error(err))
        } else {
            alert('Sijaintiasi ei onnistuttu paikantamaan!')
        }
        
    }

    clearField(e) {
        const state = this.state
        // console.log(search)
        state.search[e] = ''
        this.setState=({state})
        this.forceUpdate()
    }
    
    render() {
        const { categories, cities } = this.props
        const { mobile, search } = this.state
        return(
            cities&&categories ?
            <div>
                {/* Fullscreen */}
                <Responsive 
                    as={SearchFields}
                    fixed='top'
                    {...this.props}
                    search={search}
                    onChange={this.handleChange} 
                    getLocation={this.activeLocation}
                    clearSearch={this.clearSearch}
                    clearField={this.clearField}
                    minWidth={780} />
                {/* Mobile */}
                <Menu as={Responsive} fixed='top' size='huge' placeholder="" maxWidth={780}>
                    <Menu.Item>TapahtumaOpas</Menu.Item>
                    <Menu.Menu 
                        as={MenuIcons} 
                        onChange={this.showMenu} 
                        getLocation={this.activeLocation}
                        clearSearch={this.clearSearch}
                        position='right'/>
                </Menu>
                { mobile ?
                    <Menu stackable fixed='bottom'>
                        <SearchFields
                            search={search}
                            onChange={this.handleChange} 
                            {...this.props} 
                            /> 
                    </Menu>
                    : 
                    <div></div>}
            </div>
            :
            <Loader />
        )
    }
}

const mapStateToProps = (state) => {
    // console.log(state)
    return {
        cities: state.firestore.data.cities,
        categories: state.firestore.data.category,
        search: state.search
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        Query: (search) => dispatch(Query(search)),
        Search: (search) => dispatch(Search(search))
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
        {
            collection: 'cities'
        },
        {
            collection: 'category'
        }
    ]),
    geolocated({
        positionOptions: {
          enableHighAccuracy: false,
        },
        userDecisionTimeout: 5000,
    })
)(SearchBar)