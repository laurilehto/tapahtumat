import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Container, Grid, Responsive } from 'semantic-ui-react'

import SearchBar from '../search/SearchBar'
import List from '../events/List'
import TodayList from '../events/TodayList'
// import Footer from './Footer'

class  View extends Component {
    constructor(props){
        super(props)

        this.state = {}

        // this.updateWidth=this.updateWidth.bind(this)
    }
    

    // updateWidth(e) {
    //     this.setState({ width:e.target.window.innerWidth })
    // }

    render() {
        return (    
            <Container 
                // as={Responsive} 
                // onUpdate={this.updateWidth}
                >

                <Container as={SearchBar} style={{ marginTop: '3em' }}/>

                <Container as={Grid} style={{ marginTop: '3em' }}>
                    {/* Fullscreen */}
                    <Grid.Row as={Responsive} minWidth={780} columns={2}>
                        <Grid.Column width={12}>
                            <List />
                        </Grid.Column>
                        <Container as={Grid.Column} width={4}>
                            <TodayList {...this.props}/>
                        </Container>
                    </Grid.Row>
                    {/* Mobile */}
                    <Grid.Row as={Responsive} maxWidth={780} columns={2}>
                        <Grid.Column width={16}>
                            <List />
                        </Grid.Column>
                    </Grid.Row>
                </Container>

                {/* <Footer/> */}
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        events: state.firestore.data.events,
        query: state.query,
        search: state.search
    }
}

export default connect(mapStateToProps)(View)