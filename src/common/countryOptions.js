export const countryOptions = [
    { key: 'fi', value: 'fi', flag: 'fi', text: 'Suomi' },
    { key: 'se', value: 'se', flag: 'se', text: 'Ruotsi' },
    { key: 'no', value: 'no', flag: 'no', text: 'Norja' },
    { key: 'ee', value: 'ee', flag: 'ee', text: 'Viro' },
    { key: 'dk', value: 'dk', flag: 'dk', text: 'Tanska' }
]