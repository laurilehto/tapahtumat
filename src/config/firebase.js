import firebase from 'firebase/app'
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyDEt1fs9zRGa_AsfhhnAW_gk7Wl4vw672k",
    authDomain: "tapahtumaopas.firebaseapp.com",
    databaseURL: "https://tapahtumaopas.firebaseio.com",
    projectId: "tapahtumaopas",
    storageBucket: "tapahtumaopas.appspot.com",
    messagingSenderId: "767707233432"
};

firebase.initializeApp(firebaseConfig)
firebase.firestore().settings({ timestampsInSnapshots: true})

export default firebase